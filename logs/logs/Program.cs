﻿using System;
using System.IO;
using Services;

namespace Convert
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            if (args.Length > 1)
            {
                Console.WriteLine("Buscando dados");
                var response = ReadService.ReadByUrl(args[0]);

                Console.WriteLine("Transformando no formato csv");
                using(var stream = response.GetResponseStream())
                using (var sr = new StreamReader(stream))
                {
                    Transform transform = new Transform(sr);
                    var csv = transform.TransformToCSV();

                    Console.WriteLine("Salvando arquivo no caminho específicado no app.config");
                    using (StreamWriter sw = new StreamWriter(args[1]))
                        sw.Write(csv);
                }
            }
            else
                Console.WriteLine("Parâmetro incorreto");
        }
    }
}

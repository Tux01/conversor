﻿using System.Net;

namespace Services
{
    public class ReadService
    {
        public static WebResponse ReadByUrl(string url)
        {
            var request = WebRequest.Create(url);
            return request.GetResponse();
        }
    }
}
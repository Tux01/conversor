﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Convert
{
    public class Transform
    {
        private enum index
        {
            HTTP_METHOD = 1,
            URI = 2,
            PROTOCOL = 3
        };
        private StreamReader sr;
        private const char parameter = '|';
        private const string version = "1.0";
        private const string orign = "MINHA CDN";
        private const string methods = "GET|POST|PUT|DELETE|OPTIONS|PATCH|HEAD|TRACE";
        private const string protocols = "HTTP|HTTPS|FTP";
        private string pattern = $"^\"+({methods})\\s+(\\S)+\\s+({protocols})";

        public Transform(StreamReader sr)
        {
            this.sr = sr;
        }

        public StringBuilder TransformToCSV()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"#Version: {version}")
              .AppendLine($"#Date: {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}")
              .Append("provider;")
              .Append("http-method;")
              .Append("status-code;")
              .Append("uri-path;")
              .Append("time-taken;")
              .Append("response-size;")
              .Append("cache-status")
              .AppendLine();

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();

                if (line.IndexOf(parameter) > -1)
                {
                    sb.Append($"{orign};");
                    this.TransformLines(line, sb);

                }
            }

            return sb;
        }

        private void TransformLines(string line, StringBuilder sb)
        {
            
            var lineArray = line.Split(parameter);
            var match = Regex.Match(lineArray[3], pattern);
            var listResult = match.Groups;
            string uri = string.Empty;
            foreach(var item in listResult[index.URI.GetHashCode()].Captures)
                uri += item.ToString();
        
            sb.Append($"{listResult[index.HTTP_METHOD.GetHashCode()]};")
              .Append($"{lineArray[1]};")
              .Append($"{uri};")
              .Append($"{System.Convert.ToInt32(float.Parse(lineArray[4]))};")
              .Append($"{lineArray[0]};")
              .Append($"{lineArray[2]}")
              .AppendLine();


        }
    }
}
